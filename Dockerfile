FROM alekseyyaroslavcev/mxe-dw2-mingw-i686
MAINTAINER Aleksey Yaroslavcev <a.yaroslavcev@gmail.com>

#Make toolchain
USER devel
RUN cd /opt/mxe && \
  make qtbase qtcharts qtdeclarative qtquickcontrols qtquickcontrols2 qtscript qttools qttranslations MXE_TARGETS=i686-w64-mingw32.shared 